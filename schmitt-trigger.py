#!/usr/bin/python3
#
# Calculate the upper and lower threshold voltages
# of an opamp-based schmitt-trigger, based on a given
# voltage and resistances a, b, and c.
#
#
#    V
#    |
#    -                |\
#   | |               |  \
#   |a|       in -----| -  \
#   |_|               |      \
#    |                |      /----+------- out
#    +----------------| +  /      |
#    |                |  /        |
#    |                |/          |
#    |     ___                    |
#    +----|_b_|-------------------+
#    |
#    -
#   | |
#   |c|
#   |_|
#    |
#   GND
#

import argparse

def calclow(a,b,c):
    vat = 1 / (1/b + 1/c)
    vab = a + vat
    va = vat / vab
    return va

def calchigh(a,b,c):
    vab = b + (1 / (1/a + 1/c))
    va = b / vab
    return va

parser = argparse.ArgumentParser()
parser.add_argument('-v','--voltage', dest='v', help='Set the voltage', type=int, nargs='?', required=True, default=12)
parser.add_argument('-a','--resa', dest='a', help='Set the resistance of A', type=int, nargs='?', required=True, default=200)
parser.add_argument('-b','--resb', dest='b', help='Set the resistance of B', type=int, nargs='?', required=True, default=47)
parser.add_argument('-c','--resc', dest='c', help='Set the resistance of C', type=int, nargs='?', required=True, default=200)

args = parser.parse_args()

v = args.v
a = args.a
b = args.b
c = args.c
print("lower voltage threshold: " + str(v * calclow(a,b,c)))
print("upper voltage threshold: " + str(v * calchigh(a,b,c)))
