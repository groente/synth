\select@language {english}
\contentsline {section}{\numberline {1}A bit of theory}{3}
\contentsline {subsection}{\numberline {1.1}Sound}{3}
\contentsline {subsection}{\numberline {1.2}Oscillators}{4}
\contentsline {subsection}{\numberline {1.3}Opamps}{6}
\contentsline {section}{\numberline {2}A bit of praxis}{10}
\contentsline {subsection}{\numberline {2.1}The circuit}{10}
\contentsline {subsubsection}{\numberline {2.1.1}Legenda}{11}
\contentsline {subsection}{\numberline {2.2}Tools for the trade}{12}
\contentsline {subsection}{\numberline {2.3}Building instructions}{13}
\contentsline {section}{\numberline {3}Crisis and experiments}{14}
\contentsline {subsection}{\numberline {3.1}Fixing a noisebox}{14}
\contentsline {subsection}{\numberline {3.2}Interventions}{14}
