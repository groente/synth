EESchema Schematic File Version 4
LIBS:noisebox-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Noisebox"
Date ""
Rev "0.1"
Comp "Nomen Nescio"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 4xxx:40106 U1
U 7 1 5CC4A1A0
P 2100 1650
F 0 "U1" H 2330 1696 50  0000 L CNN
F 1 "40106" H 2330 1605 50  0000 L CNN
F 2 "" H 2100 1650 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/HEF40106B.pdf" H 2100 1650 50  0001 C CNN
	7    2100 1650
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:40106 U1
U 1 1 5CC4A224
P 3600 1500
F 0 "U1" H 3600 1817 50  0000 C CNN
F 1 "40106" H 3600 1726 50  0000 C CNN
F 2 "" H 3600 1500 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/HEF40106B.pdf" H 3600 1500 50  0001 C CNN
	1    3600 1500
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:40106 U1
U 2 1 5CC4A279
P 3600 3000
F 0 "U1" H 3600 3317 50  0000 C CNN
F 1 "40106" H 3600 3226 50  0000 C CNN
F 2 "" H 3600 3000 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/HEF40106B.pdf" H 3600 3000 50  0001 C CNN
	2    3600 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C1
U 1 1 5CC4A3E4
P 3100 1650
F 0 "C1" H 3215 1696 50  0000 L CNN
F 1 ".47uF" H 3215 1605 50  0000 L CNN
F 2 "" H 3100 1650 50  0001 C CNN
F 3 "~" H 3100 1650 50  0001 C CNN
	1    3100 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C2
U 1 1 5CC4A432
P 3100 3150
F 0 "C2" H 3215 3196 50  0000 L CNN
F 1 ".47uF" H 3215 3105 50  0000 L CNN
F 2 "" H 3100 3150 50  0001 C CNN
F 3 "~" H 3100 3150 50  0001 C CNN
	1    3100 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0101
U 1 1 5CC4A4CA
P 3100 1800
F 0 "#PWR0101" H 3100 1600 50  0001 C CNN
F 1 "GNDPWR" H 3104 1646 50  0000 C CNN
F 2 "" H 3100 1750 50  0001 C CNN
F 3 "" H 3100 1750 50  0001 C CNN
	1    3100 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0102
U 1 1 5CC4A4FB
P 3100 3300
F 0 "#PWR0102" H 3100 3100 50  0001 C CNN
F 1 "GNDPWR" H 3104 3146 50  0000 C CNN
F 2 "" H 3100 3250 50  0001 C CNN
F 3 "" H 3100 3250 50  0001 C CNN
	1    3100 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0103
U 1 1 5CC4A52C
P 2100 2150
F 0 "#PWR0103" H 2100 1950 50  0001 C CNN
F 1 "GNDPWR" H 2104 1996 50  0000 C CNN
F 2 "" H 2100 2100 50  0001 C CNN
F 3 "" H 2100 2100 50  0001 C CNN
	1    2100 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1500 3300 1500
Wire Wire Line
	3100 3000 3300 3000
$Comp
L power:+BATT #PWR0104
U 1 1 5CC4A5D0
P 2100 1150
F 0 "#PWR0104" H 2100 1000 50  0001 C CNN
F 1 "+BATT" H 2115 1323 50  0000 C CNN
F 2 "" H 2100 1150 50  0001 C CNN
F 3 "" H 2100 1150 50  0001 C CNN
	1    2100 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 5CC4A870
P 3100 1350
F 0 "RV1" H 3030 1396 50  0000 R CNN
F 1 "10k log" H 3030 1305 50  0000 R CNN
F 2 "" H 3100 1350 50  0001 C CNN
F 3 "~" H 3100 1350 50  0001 C CNN
	1    3100 1350
	1    0    0    -1  
$EndComp
Connection ~ 3100 1500
Wire Wire Line
	3250 1350 3250 1050
Wire Wire Line
	3250 1050 3900 1050
Wire Wire Line
	3900 1050 3900 1500
$Comp
L Device:R_PHOTO LDR1
U 1 1 5CC4A915
P 3100 2650
F 0 "LDR1" H 3170 2696 50  0000 L CNN
F 1 "GL5539" H 3170 2605 50  0000 L CNN
F 2 "" V 3150 2400 50  0001 L CNN
F 3 "~" H 3100 2600 50  0001 C CNN
	1    3100 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 2800 3100 3000
Connection ~ 3100 3000
Wire Wire Line
	3100 2500 3900 2500
$Comp
L Switch:SW_SPST SW1
U 1 1 5CC4AB25
P 3500 2350
F 0 "SW1" H 3500 2585 50  0000 C CNN
F 1 "SW_SPST" H 3500 2494 50  0000 C CNN
F 2 "" H 3500 2350 50  0001 C CNN
F 3 "" H 3500 2350 50  0001 C CNN
	1    3500 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5CC4AC35
P 3900 1900
F 0 "C3" H 4015 1946 50  0000 L CNN
F 1 ".1uF" H 4015 1855 50  0000 L CNN
F 2 "" H 3938 1750 50  0001 C CNN
F 3 "~" H 3900 1900 50  0001 C CNN
	1    3900 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1750 3900 1500
Connection ~ 3900 1500
Wire Wire Line
	3900 2050 3900 2350
Wire Wire Line
	3900 2350 3700 2350
Wire Wire Line
	3300 2350 2800 2350
Wire Wire Line
	2800 2350 2800 3000
Wire Wire Line
	2800 3000 3100 3000
$Comp
L Switch:SW_SPDT SW2
U 1 1 5CC4B12B
P 3700 3750
F 0 "SW2" H 3700 4035 50  0000 C CNN
F 1 "SW_SPDT" H 3700 3944 50  0000 C CNN
F 2 "" H 3700 3750 50  0001 C CNN
F 3 "" H 3700 3750 50  0001 C CNN
	1    3700 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3850 4200 3850
Wire Wire Line
	4200 3850 4200 1500
Wire Wire Line
	4200 1500 3900 1500
$Comp
L Device:R R3
U 1 1 5CC4B8E3
P 5100 4050
F 0 "R3" H 5170 4096 50  0000 L CNN
F 1 "10k" H 5170 4005 50  0000 L CNN
F 2 "" V 5030 4050 50  0001 C CNN
F 3 "~" H 5100 4050 50  0001 C CNN
	1    5100 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0106
U 1 1 5CC4C636
P 4550 3850
F 0 "#PWR0106" H 4550 3650 50  0001 C CNN
F 1 "GNDPWR" H 4554 3696 50  0000 C CNN
F 2 "" H 4550 3800 50  0001 C CNN
F 3 "" H 4550 3800 50  0001 C CNN
	1    4550 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5CC4C686
P 4550 3700
F 0 "R2" H 4620 3746 50  0000 L CNN
F 1 "1k" H 4620 3655 50  0000 L CNN
F 2 "" V 4480 3700 50  0001 C CNN
F 3 "~" H 4550 3700 50  0001 C CNN
	1    4550 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_PHOTO LDR2
U 1 1 5CC4C6F5
P 4650 4300
F 0 "LDR2" H 4720 4346 50  0000 L CNN
F 1 "GL5539" H 4720 4255 50  0000 L CNN
F 2 "" V 4700 4050 50  0001 L CNN
F 3 "~" H 4650 4250 50  0001 C CNN
	1    4650 4300
	0    1    1    0   
$EndComp
$Comp
L Amplifier_Audio:LM386 U2
U 1 1 5CC4E1E7
P 6500 3500
F 0 "U2" H 6841 3546 50  0000 L CNN
F 1 "LM386" H 6841 3455 50  0000 L CNN
F 2 "" H 6600 3600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm386.pdf" H 6700 3700 50  0001 C CNN
	1    6500 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5CC4E4D4
P 6600 3950
F 0 "C4" H 6715 3996 50  0000 L CNN
F 1 ".1uF" H 6715 3905 50  0000 L CNN
F 2 "" H 6638 3800 50  0001 C CNN
F 3 "~" H 6600 3950 50  0001 C CNN
	1    6600 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3800 6500 4100
Wire Wire Line
	6500 4100 6600 4100
$Comp
L power:+BATT #PWR0108
U 1 1 5CC4EB4E
P 6400 3200
F 0 "#PWR0108" H 6400 3050 50  0001 C CNN
F 1 "+BATT" H 6415 3373 50  0000 C CNN
F 2 "" H 6400 3200 50  0001 C CNN
F 3 "" H 6400 3200 50  0001 C CNN
	1    6400 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0109
U 1 1 5CC4EBAA
P 6200 3800
F 0 "#PWR0109" H 6200 3600 50  0001 C CNN
F 1 "GNDPWR" H 6204 3646 50  0000 C CNN
F 2 "" H 6200 3750 50  0001 C CNN
F 3 "" H 6200 3750 50  0001 C CNN
	1    6200 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3800 6200 3800
Wire Wire Line
	6200 3800 6200 3600
Connection ~ 6200 3800
$Comp
L Device:R_POT RV2
U 1 1 5CC4F9CF
P 6600 2500
F 0 "RV2" H 6530 2546 50  0000 R CNN
F 1 "50k" H 6530 2455 50  0000 R CNN
F 2 "" H 6600 2500 50  0001 C CNN
F 3 "~" H 6600 2500 50  0001 C CNN
	1    6600 2500
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDPWR #PWR0110
U 1 1 5CC4FAC1
P 6100 2500
F 0 "#PWR0110" H 6100 2300 50  0001 C CNN
F 1 "GNDPWR" H 6104 2346 50  0000 C CNN
F 2 "" H 6100 2450 50  0001 C CNN
F 3 "" H 6100 2450 50  0001 C CNN
	1    6100 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2500 6800 2500
$Comp
L Connector:AudioJack2_SwitchT J1
U 1 1 5CC4FFE5
P 6600 1950
F 0 "J1" V 6584 2138 50  0000 L CNN
F 1 "minijack" V 6675 2138 50  0000 L CNN
F 2 "" H 6600 1950 50  0001 C CNN
F 3 "~" H 6600 1950 50  0001 C CNN
	1    6600 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	6600 2350 6600 2150
$Comp
L power:GNDPWR #PWR0111
U 1 1 5CC509F6
P 6850 2150
F 0 "#PWR0111" H 6850 1950 50  0001 C CNN
F 1 "GNDPWR" H 6854 1996 50  0000 C CNN
F 2 "" H 6850 2100 50  0001 C CNN
F 3 "" H 6850 2100 50  0001 C CNN
	1    6850 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2150 6850 2150
$Comp
L Device:Speaker LS1
U 1 1 5CC50E45
P 6500 1350
F 0 "LS1" V 6511 1070 50  0000 R CNN
F 1 "Speaker" V 6420 1070 50  0000 R CNN
F 2 "" H 6500 1150 50  0001 C CNN
F 3 "~" H 6490 1300 50  0001 C CNN
	1    6500 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6500 2150 6300 2150
Wire Wire Line
	6300 2150 6300 1550
Wire Wire Line
	6300 1550 6500 1550
Wire Wire Line
	6600 1550 7200 1550
Wire Wire Line
	7200 1550 7200 2150
Wire Wire Line
	7200 2150 6850 2150
Connection ~ 6850 2150
Connection ~ 3900 3000
Wire Wire Line
	3900 3000 3900 3650
Wire Wire Line
	3900 2500 3900 3000
$Comp
L Device:CP1 C5
U 1 1 5CEA8EAD
P 6800 3350
F 0 "C5" H 6915 3396 50  0000 L CNN
F 1 "220uF" H 6915 3305 50  0000 L CNN
F 2 "" H 6800 3350 50  0001 C CNN
F 3 "~" H 6800 3350 50  0001 C CNN
	1    6800 3350
	1    0    0    1   
$EndComp
Wire Wire Line
	6800 3200 6800 2500
$Comp
L Amplifier_Operational:LM324 U3
U 1 1 5CEAE31E
P 5400 3650
F 0 "U3" H 5400 4017 50  0000 C CNN
F 1 "LM324" H 5400 3926 50  0000 C CNN
F 2 "" H 5350 3750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 5450 3850 50  0001 C CNN
	1    5400 3650
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324 U3
U 5 1 5CEAE887
P 1600 1650
F 0 "U3" H 1558 1696 50  0000 L CNN
F 1 "LM324" H 1558 1605 50  0000 L CNN
F 2 "" H 1550 1750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 1650 1850 50  0001 C CNN
	5    1600 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 1350 1500 1150
Wire Wire Line
	1500 1150 2100 1150
Connection ~ 2100 1150
Wire Wire Line
	1500 1950 1500 2150
Wire Wire Line
	1500 2150 2100 2150
Connection ~ 2100 2150
$Comp
L Device:R R1
U 1 1 5CEB05E0
P 4550 3400
F 0 "R1" H 4620 3446 50  0000 L CNN
F 1 "1k" H 4620 3355 50  0000 L CNN
F 2 "" V 4480 3400 50  0001 C CNN
F 3 "~" H 4550 3400 50  0001 C CNN
	1    4550 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5CEB0CC6
P 4550 3250
F 0 "#PWR?" H 4550 3100 50  0001 C CNN
F 1 "+BATT" H 4565 3423 50  0000 C CNN
F 2 "" H 4550 3250 50  0001 C CNN
F 3 "" H 4550 3250 50  0001 C CNN
	1    4550 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3550 5100 3550
Connection ~ 4550 3550
Wire Wire Line
	5100 3900 5100 3750
$Comp
L Device:R_POT_TRIM RV3
U 1 1 5CEB4163
P 5700 4200
F 0 "RV3" H 5630 4246 50  0000 R CNN
F 1 "R_POT_TRIM 100K" H 5630 4155 50  0000 R CNN
F 2 "" H 5700 4200 50  0001 C CNN
F 3 "~" H 5700 4200 50  0001 C CNN
	1    5700 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5700 3650 5700 4050
Wire Wire Line
	5550 4200 5100 4200
Wire Wire Line
	4800 4300 4900 4300
Wire Wire Line
	4900 4300 4900 3750
Wire Wire Line
	4900 3750 5100 3750
Connection ~ 5100 3750
Wire Wire Line
	5700 3650 5700 3400
Wire Wire Line
	5700 3400 6200 3400
Connection ~ 5700 3650
Wire Wire Line
	3500 4300 4500 4300
Wire Wire Line
	3500 3750 3500 4300
Wire Wire Line
	6100 2500 6450 2500
$EndSCHEMATC
